// HomeWork_17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <math.h>

class CTestClass
{
private:
    char info [100] = "test class";

public:
    void PrintInfo()
    {
        std::cout << info << '\n';
    }
};

class Vector
{
private:
    double x, y, z;
public:
    double md;
    //
    Vector() : Vector(0, 0, 0) {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) { Module(); }
    double Module()
    {
        md = sqrt(x*x + y*y + z*z);
        return md;
    }
};

int main()
{
    CTestClass tc;
    tc.PrintInfo();
    //
    Vector v1;
    Vector v2(1, 2, 3);

    std::cout << "v1:" << v1.md << '\n';
    std::cout << "v2:" << v2.md << '\n';

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
